package fr.guenezan.framework.data;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Hashtable;

public class Data {

	public static Hashtable<String, Data> file = new Hashtable<String, Data>();
	private String name;
	public String path;
	public Hashtable<String, String> data;
	public String[] sd;
	
	private Data(File file, String name) {
		path = file.getPath();
		data = new Hashtable<String, String>();
		this.name = name;
		try {
			@SuppressWarnings("resource")
			FileChannel fc = new FileInputStream(file).getChannel();
			int size = (int) fc.size();
			ByteBuffer bBuff = ByteBuffer.allocate(size);
			fc.read(bBuff);
			bBuff.flip();
			sd = new String(bBuff.array()).split("\n");
			fc.close();
			bBuff.clear();

			for (int i = 0; i < sd.length; i++) {
				if (sd[i].startsWith("//") || sd[i].isEmpty() || sd[i].indexOf("=") == -1) {
				} else {
					String key = stringSub(sd[i].substring(0, sd[i].indexOf("=")));
					String value = stringSub(sd[i].substring(sd[i].indexOf("=") + 1, sd[i].length()));
//					if (value.indexOf(",") != -1) {
//						value = value.substring(0, value.indexOf(",")) + "."
//								+ value.substring(value.indexOf(",") + 1, value.length());
//					}
					data.put(key, value);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void addValue(String key, String value) {
		data.put(key, value);
		String[] sdT = new String[sd.length + 1];
		for (int i = 0; i < sd.length; i++) {
			sdT[i] = sd[i];
		}
		sdT[sd.length] = key + " = " + value;
		sd = sdT.clone();
		save();
	}

	public void addValueNoSave(String key, String value) {
		data.put(key, value);
		String[] sdT = new String[sd.length + 1];
		for (int i = 0; i < sd.length; i++) {
			sdT[i] = sd[i];
		}
		sdT[sd.length] = key + " = " + value;
		sd = sdT.clone();
	}

	public void setValue(String key, String value) {
		if (data.containsKey(key)) {
			data.remove(key);
			data.put(key, value);
			for (int i = 0; i < sd.length; i++) {
				if (sd[i].startsWith("//") || sd[i].isEmpty() || sd[i].indexOf("=") == -1) {

				} else {
					if (stringSub(sd[i].substring(0, sd[i].indexOf("="))).equals(key)) {
						sd[i] = key + " = " + value;
					}
				}
			}
			save();
		} else {
			addValue(key, value);
		}
	}
	
	public void setValueNoSave(String key, String value) {
		if (data.containsKey(key)) {
			data.remove(key);
			data.put(key, value);
			for (int i = 0; i < sd.length; i++) {
				if (sd[i].startsWith("//") || sd[i].isEmpty() || sd[i].indexOf("=") == -1) {

				} else {
					if (stringSub(sd[i].substring(0, sd[i].indexOf("="))).equals(key)) {
						sd[i] = key + " = " + value;
					}
				}
			}
		} else {
			addValueNoSave(key, value);
		}
	}

	public void removeValue(String key) {
		try {
			data.remove(key);
			String[] sdT = new String[sd.length - 1];
			int a = 0;
			for (int i = 0; i < sd.length; i++) {
				if (sd[i].startsWith("//") || sd[i].isEmpty() || sd[i].indexOf("=") == -1) {
					sdT[a] = sd[i];
					a++;
				} else {
					if (stringSub(sd[i].substring(0, sd[i].indexOf("="))).equals(key)) {
					} else {
						sdT[a] = sd[i];
						a++;
					}
				}
			}
			sd = sdT.clone();
			save();
		} catch (Exception e) {
			System.err.println(key + " doesn't exist ! Remove cancel");
		}
	}
	
	public void removeValueNoSave(String key) {
		try {
			data.remove(key);
			String[] sdT = new String[sd.length - 1];
			int a = 0;
			for (int i = 0; i < sd.length; i++) {
				if (sd[i].startsWith("//") || sd[i].isEmpty() || sd[i].indexOf("=") == -1) {
					sdT[a] = sd[i];
					a++;
				} else {
					if (stringSub(sd[i].substring(0, sd[i].indexOf("="))).equals(key)) {
					} else {
						sdT[a] = sd[i];
						a++;
					}
				}
			}
			sd = sdT.clone();
		} catch (Exception e) {
			System.err.println(key + " doesn't exist ! Remove cancel");
		}
	}

	public void save() {
		try {
			File f = new File(path);
			f.delete();
			f.createNewFile();
			@SuppressWarnings("resource")
			FileChannel fc = new FileOutputStream(f).getChannel();
			String tmp = "";
			for (int i = 0; i < sd.length; i++) {
				tmp += sd[i] + "\n";
			}
			ByteBuffer byteBuffer = ByteBuffer.wrap(tmp.getBytes());
			fc.write(byteBuffer);
			fc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void removeDate(String key) {
		file.remove(key);
	}

	public void removeDate() {
		file.remove(name);
		try {
			finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public static Data getData(String name) {
		if (file.get(name) == null) {
			File f = new File(name + ".dat");
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			file.put(name, new Data(f, name));
		}
		return file.get(name);
	}

	public String getValue(String key) {
		return data.get(key);
	}



	private static String stringSub(String in) {
		boolean c = true;
		while (c) {
			if (in.startsWith(" ") || in.startsWith("\r")) {
				in = in.substring(1, in.length());
			} else {
				c = false;
			}
		}
		c = true;
		while (c) {
			if (in.endsWith(" ") || in.endsWith("\r")) {
				in = in.substring(0, in.length() - 1);
			} else {
				c = false;
			}
		}
		return in;
	}

	public String getName() {
		return name;
	}
}