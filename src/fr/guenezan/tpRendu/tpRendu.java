package fr.guenezan.tpRendu;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.guenezan.framework.data.Data;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.JLabel;

public class tpRendu {
	private static JTextField txtGuenezp;
	private static JTextField textField;

	private static File[] source;

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException, URISyntaxException, IOException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

		final JFrame frmSubmission = new JFrame();
		frmSubmission.setTitle("Submission generator");
		frmSubmission.setSize(468, 144);
		frmSubmission.setLocationRelativeTo(null);
		frmSubmission.getContentPane().setLayout(null);

		File fData = new File("setting.dat");
		if (!fData.exists()) {
			Files.write(fData.toPath(), ("login = guenez_p\ntpName = tpcamlX\npath = C:\\\n").getBytes(), StandardOpenOption.CREATE_NEW);
		}
		final Data data = Data.getData("setting");
		final JButton btnNewButton = new JButton("Generate");
		btnNewButton.setEnabled(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String login = txtGuenezp.getText().trim(), tpName = textField.getText().trim();
					data.setValue("login", login);
					data.setValue("tpName", tpName);
					File tmpFolder = new File("tmp");
					tmpFolder.mkdir();
					File mainRoot = new File(tmpFolder.getAbsolutePath() + "\\" + login + "-" + tpName);
					mainRoot.mkdir();
					File authors = new File(mainRoot.getAbsolutePath() + "\\AUTHORS");
					Files.write(authors.toPath(), ("* " + login + "\n").getBytes(), StandardOpenOption.CREATE_NEW);
					File src = new File(mainRoot.getAbsolutePath() + "\\src");
					src.mkdir();
					boolean asREADME = false;
					for (File f : source) {
						if (f.getName().equals("README")) {
							asREADME = true;
							Files.copy(f.toPath(), new File(mainRoot.getAbsolutePath() + "\\README").toPath(),
									StandardCopyOption.REPLACE_EXISTING);

						} else {
							Files.copy(f.toPath(), new File(src.getAbsolutePath() + "\\" + f.getName()).toPath(),
									StandardCopyOption.REPLACE_EXISTING);
						}
					}
					if (!asREADME) {
						Files.write(new File(mainRoot.getAbsolutePath() + "\\README").toPath(),
								"No README was selected when the user generate the zip :)\n".getBytes(), StandardOpenOption.CREATE_NEW);
					}
					Utils.zipIt(login + "-" + tpName + ".zip", login + "-" + tpName + ".zip", mainRoot.getAbsolutePath());
					Utils.removeDirectory(tmpFolder);
					frmSubmission.dispose();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(268, 10, 174, 23);
		frmSubmission.getContentPane().add(btnNewButton);

		txtGuenezp = new JTextField();
		txtGuenezp.setText(data.getValue("login"));
		txtGuenezp.setBounds(50, 42, 392, 20);
		frmSubmission.getContentPane().add(txtGuenezp);
		txtGuenezp.setColumns(10);

		JLabel lblLogin = new JLabel("Login :");
		lblLogin.setBounds(10, 42, 53, 20);
		frmSubmission.getContentPane().add(lblLogin);

		JLabel lblTpName = new JLabel("TP Name : ");
		lblTpName.setBounds(10, 73, 200, 20);
		frmSubmission.getContentPane().add(lblTpName);

		textField = new JTextField(data.getValue("tpName"));
		textField.setBounds(72, 73, 370, 20);
		frmSubmission.getContentPane().add(textField);
		textField.setColumns(10);

		JButton btnSelectSources = new JButton("Select sources and README ( optional )");
		btnSelectSources.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser(data.getValue("path"));
				chooser.setMultiSelectionEnabled(true);
				if (chooser.showOpenDialog(frmSubmission) == JFileChooser.APPROVE_OPTION) {
					source = chooser.getSelectedFiles();
					data.setValue("path", source[0].getParent());
					btnNewButton.setEnabled(true);
				}
			}
		});
		btnSelectSources.setBounds(10, 10, 248, 23);
		frmSubmission.getContentPane().add(btnSelectSources);
		frmSubmission.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSubmission.setVisible(true);

	}
}
